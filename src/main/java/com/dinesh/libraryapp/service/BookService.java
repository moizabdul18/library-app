package com.dinesh.libraryapp.service;

import com.dinesh.libraryapp.model.Book;

import java.util.List;

public interface BookService {

    public void createBook(Book book);

    public void updateBook(Book book);

    Book getBooks(String id);

    List<Book> getBooks();

    void deleteBookById(String id);

    void deleteAll();

    void deleteMultiple(List<String> idsList);
}
