package com.dinesh.libraryapp.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Book {

    @Id
    private String id;
    private String author;
    private String isbn;
    private String cost;
}
