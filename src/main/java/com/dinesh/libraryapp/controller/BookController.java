package com.dinesh.libraryapp.controller;

import com.dinesh.libraryapp.model.Book;
import com.dinesh.libraryapp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/library")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping
    public void createBook(@RequestBody Book book) {
        bookService.createBook(book);
    }

    @PutMapping
    public void updateBook(@RequestBody Book book) {
        bookService.updateBook(book);
    }

    @GetMapping
    public Book getBookById(@RequestParam("id") String id) {
        return bookService.getBooks(id);
    }

    @GetMapping(path = "/all")
    public List<Book> getAllBook() {
        return bookService.getBooks();
    }

    @DeleteMapping
    public void deleteBook(@RequestParam("id") String id) {
        bookService.deleteBookById(id);
    }

    @DeleteMapping(path = "/all")
    public void deleteAll() {
        bookService.deleteAll();
    }

    @DeleteMapping(path = "/multiple")
    public void deleteMultiple(@RequestBody List<String> idsList) {
        bookService.deleteMultiple(idsList);
    }
}
